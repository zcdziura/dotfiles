"
" File:     .vimrc
" Author:   Zach Dziura <zcdziura@gmail.com>
" Modified: 2015 Jun 30
"

" Use Vim-only settings
set nocompatible

" Enable Vundle
filetype off

filetype plugin indent on

" Allow backspacing over everything while in Insert Mode
set backspace=indent,eol,start

set history=1000
set number
set ruler
set showcmd
set incsearch
set hlsearch
set linebreak
set scrolloff=3
set splitright
set timeoutlen=100
set fillchars=fold:\ ,
set tildeop
if exists('&breakindent')
  set breakindent
endif

" Syntax highlighting; any themes will go here
syntax enable

" Tab settings
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2

" Enable the mouse
if has('mouse')
  set mouse=a
endif
